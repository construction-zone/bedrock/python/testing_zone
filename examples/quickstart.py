"""
This example script imports the testing_zone package and
prints out the version.
"""

import testing_zone


def main():
    print(f"testing_zone version: {testing_zone.__version__}")


if __name__ == "__main__":
    main()
