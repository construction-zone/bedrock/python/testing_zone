import testing_zone


def test___init__():
    """check testing_zone exposes a version attribute"""
    assert hasattr(testing_zone, "__version__")
    assert isinstance(testing_zone.__version__, str)
